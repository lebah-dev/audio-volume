function startAudio() {
	let audio = document.getElementById('audio');
	let score = document.getElementById('score');
	let notif = document.getElementById('notif');

	document.querySelectorAll('.modal .alert button')
		.forEach(function (button) {
			button.addEventListener('click', function () {
				document.querySelectorAll('.modal').forEach(function (modal) {
					modal.style = 'none';
				})
			})
		})

	audio.volume = 0.6;
	audio.play();

	audio.addEventListener('ended', function () {
		document.querySelector('#score.modal .alert h1')
			.innerHTML = audio.volume <= 0.6
				? parseInt(audio.volume / 0.6 * 100)
				: parseInt((1 - audio.volume) / 0.4 * 100);
		score.style.display = 'flex';
	});

	audio.addEventListener('volumechange', function () {
		if (audio.volume > 0.6) {
			notif.style.display = 'flex';
		}
	})
}

startAudio();